
Feature: Home screen test cases

@smoke
  Scenario: Home belts loaded successfully, Back To top works
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And element 'Hero Metadata' exists
    And element 'Hero Rating' exists
    And element 'Hero Moods' exists
    And element 'Hero Synopsis' exists
    And element 'Featured Belt Title' exists
    And 'Top Picks' navigation item is focused
    And element 'Banner Image' is loaded
    When user presses Down exactly 1 times
    And element 'Banner Image' is loaded
    When user presses Down exactly 2 times
    And element 'Banner Image' is loaded
    Then element 'Explore Channels' exists
    When user presses Down every 1 seconds exactly 10 times
    Then element 'TV1' exists
    And element 'Live Progress Bar' exists
    And I navigate to 'Back To Home' element by pressing 'Down' times until it is focused
    And element 'Back To Home' contains text content 'Back to Top'
    And 'Top Picks' navigation item is focused
    And element 'Banner Image' is loaded

  Scenario: User can navigate to branded channels screen from home and back to home screen
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And I navigate to 'Branded Tile' element by pressing 'Down' times until it is focused
    And I long wait for element 'Brand Logo' exists on 'BVOD' screen
    And 'First Item' navigation item is focused
    And user presses Enter every 2 seconds exactly 1 times
    And element 'Similar Shows' contains text content 'YOU MIGHT LIKE'
    When user presses Back exactly 1 times
    And 'First Item' navigation item is focused
    When user presses Back exactly 1 times
    Then 'Branded Tile' navigation item is focused
  
  @home1
  Scenario: Verify User can navigate to Live TV from home
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And I navigate to 'TV1' element by pressing 'Down' times until it is focused
    And user presses Down every 3 seconds exactly 1 times
    And I long wait for element 'Episode Grid EPG' exists on 'Live' screen

  
  Scenario: Verify More tile on Continue Watching Home screen navigates to categories and back from there
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And  I navigate to 'More' element by pressing 'Right' times until it is focused
    Then element "Category Menu" exists
    And element 'Banner Image' is loaded
    And 'First Item' navigation item is focused
    When user presses Back exactly 1 times
    Then 'More' navigation item is focused

  Scenario: Verify More tile on Favourites Home screen navigates to Favourites and back from there
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Favourites First Tile' element by pressing 'Down' times until it is focused
    And  I moved over to 'More' element by pressing 'Right' times until it is selected
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Screen Header' has text content 'My List'
    And element 'Hero Image' exists
    And 'First Item' navigation item is focused
    And user presses Back every 2 seconds exactly 1 times
    And 'More' navigation item is focused

  @home
  Scenario: Play button is updated with Resume play to continue watching the show
    When user presses Right every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 3 seconds exactly 1 times
    Then element 'Resume Playing Label' contains text content 'Resume'
    And 'Play Button' navigation item is focused

  @smoke
  Scenario: User can navigate to categories screen from Home
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Categories First Tile' element by pressing 'Down' times until it is focused
   And I verify each categories by pressing 'Enter''Right''Back''Right' to validate 'Banner Image''Screen Header'

    @smoke
  Scenario: Verify More tile on Movies Home screen navigates to Movies and back from there
     When user presses Enter every 2 seconds exactly 1 times
     And I long wait for element 'Hero Belt' exists on 'Home' screen
     And  I moved over to 'Movies First Tile' element by pressing 'Down' times until it is focused
     And  I navigate to 'More' element by pressing 'Right' times until it is focused
     And user presses Enter every 2 seconds exactly 1 times
     Then element 'Movies Title' has text content 'Movies'
    And user presses Back every 2 seconds exactly 1 times
     And 'More' navigation item is focused


    @smoke
  Scenario: LG : Home belts loaded successfully, Back To top works using LG mouse
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And 'Top Picks' navigation item is focused
    And I scroll from element 'Top Picks' to 'down'
    And I move to element 'Continue Watching First Tile'
    And I scroll from element 'Continue Watching First Tile' to 'down'
    And I move to element 'Explore Channels'
    And I scroll from element 'Continue Watching First Tile' to 'down'
    And I navigate to 'Back To Home' element by pressing 'Down' times until it is focused
    And I click on element 'Back To Home'
    And 'Top Picks' navigation item is focused