 @smoke
Feature: Search feature
 Search test cases on different profiles
 
  
 @smoke
  Scenario: Search and not found
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And element 'Search Result Title' has text content 'Popular Searches'
   And element 'First Item' exists
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Search Result Title' has text content '0 results for "abc"'
   And element 'First Item' does not exist
   
@smoke
@test
  Scenario: Clear search removes search result
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left every 2 seconds exactly 2 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Left every 2 seconds exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Search Field' has text content 'Search'
   Then element 'Input Field' has text content 'nurs'
   Then element 'Search Result Title' has text content '2 results for "nurs"'
   And user presses Down every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Input Field' has text content ''
   Then element 'Search Result Title' has text content 'Popular Searches'


   @smoke
  Scenario: Search on Kids profile for Adult show
   When user presses Right exactly 2 times
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Left exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Right exactly 7 times
   And user presses Enter
   And user presses Right exactly 1 times
   And user presses Enter
   Then element 'Search Result Title' has text content '0 results for "nurs"'

  Scenario: Search For Movies category
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Left exactly 3 times
   And user presses Enter
   And user presses Right exactly 6 times
   And user presses Enter
   Then element 'Hub Header' has text content 'Movies'

  Scenario: Search For tv shows category
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 9 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Left exactly 5 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 6 times
   And user presses Enter
   Then element 'Hub Header' has text content 'TV Shows'
