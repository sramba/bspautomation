
Feature: Smoke test
  Test includes login related test cases from application
  @logout
  Scenario: Verify login with invalid credentials
   When element 'Logo' is loaded
   When user presses Down every 1 seconds exactly 2 times
   And user presses Right every 1 seconds exactly 4 times
   And user presses Enter
   And user presses Right every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Left every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Down
   And user presses Enter
   And user presses Down every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Enter every 1 seconds exactly 5 times
   And user presses down every 1 seconds exactly 4 times
   And user presses Right
   And user presses Enter every 4 seconds exactly 1 times
   Then element 'Error Message' contains text content 'Please try again'
   And element 'Error Message' contains text content 'The username or password entered was incorrect.'
   And user presses Enter
   Then element 'Email' exists
   And element 'Login Button' exists
   
@v2
  @logout 
  Scenario:  Unverified account user not able to see vod
   When user presses Right exactly 1 times
    And user presses Enter
    When user presses Down exactly 2 times
    And user presses Right exactly 6 times
    And user presses Enter
    And user presses Up exactly 2 times
    And user presses Left exactly 7 times
    And user presses Enter
    And user presses Down exactly 3 times
    And user presses Right exactly 2 times
    And user presses Enter
    And user presses Down exactly 3 times
    And user presses Enter
    And user presses Up exactly 7 times
    And user presses Enter exactly 8 times
    And element 'Email' contains text content 'b@a.com'
    And element 'Password' contains text content '********'
    And user presses Down exactly 5 times
    And user presses Right exactly 1 times
    And user presses Enter
    Then 'Ok Button' navigation item is focused
    And element 'Error Message' contains text content 'Please check your b@a.com inbox for an email from us'
    And element 'Error Message' contains text content 'Try checking your junk or spam folder, or make sure you have entered the correct email address.'
    And element 'Resend Email' exists
    And user presses Enter every 2 seconds exactly 3 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Enter every 2 seconds exactly 2 times
    And element 'Error Message' contains text content 'Verify your account to watch this video'

@logout 
  Scenario: Verify login with valid account
   When element 'Logo' is loaded
   When I have get network request to 'Screen' which contains 'loginpage' is made
   When user presses Down every 1 seconds exactly 2 times
   And user presses Right every 1 seconds exactly 4 times
   And user presses Enter
   And user presses Right every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Left every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Down
   And user presses Enter
   And user presses Down every 1 seconds exactly 3 times
   And user presses Enter
   And user presses Enter every 1 seconds exactly 8 times
   And user presses down every 1 seconds exactly 4 times
   And user presses Right
   Then element 'Email' contains text content 'y@y.com'
   And element 'Password' contains text content '********'
   And user presses Enter every 4 seconds exactly 1 times
   And element 'Profile Promo' exists
 
  


     