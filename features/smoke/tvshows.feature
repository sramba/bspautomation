Feature: To verify tv shows screen testcases
Scenario: Navigate to tv shows hub, Back To top tile works
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 2 times
    And 'TV Shows Icon' navigation item is focused
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Hub Header' has text content 'TV Shows'
    And 'First Item' navigation item is selected
    And element 'Hero Metadata' exists
    And element 'Hero Rating' exists
    And element 'Hero Moods' exists
    And element 'Hero Synopsis' exists
    And I navigate to 'Hub_Back_To_Top' element by pressing 'Down' times until it is focused
    And element 'Hub_Back_To_Top' contains text content 'Back To Top'
    And 'First Item' navigation item is selected
    And element 'Banner Image' is loaded

    Scenario: Navigate to subhub from More Kids on TV Shows
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And I navigate to 'TV Shows Icon' element by pressing 'Down' times until it is focused
    Then element 'Hub Header' has text content 'TV Shows'
    And I moved over to 'Kids First Tile' element by pressing 'Down' times until it is focused
    And  I navigate to 'More' element by pressing 'Right' times until it is focused
    And 'First Item' navigation item is focused
    When user presses Back exactly 1 times
    Then 'More' navigation item is focused