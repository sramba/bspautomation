
Feature: Show screen test cases
Test cases that validates show screen behavior

  Scenario: Show metadata is validated on show page for Movies
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left every 2 seconds exactly 2 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Right every 1 seconds exactly 9 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Left every 1 seconds exactly 9 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Right every 1 seconds exactly 2 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Left every 1 seconds exactly 2 times
   And user presses Down every 1 seconds exactly 1 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Right every 1 seconds exactly 8 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Left every 1 seconds exactly 4 times
   And user presses Enter every 1 seconds exactly 1 times
   And user presses Right every 1 seconds exactly 6 times
   And user presses Enter every 1 seconds exactly 1 times
   Then element 'Title' has text content 'Jackie'
   And element 'Rating' has text content 'MVLC'
   And element 'Metadata' contains text content 'Drama'
   And element 'Metadata' contains text content '1 HR 34 MINS'
   And element 'Moods' contains text content 'Powerful, Iconic'
   And element 'Synopsis' contains text content 'Jacqueline'

  Scenario: Show metadata is validated on show page ,Episode can be played from episodes screen 
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left every 2 seconds exactly 2 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 5 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Right exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Left every 2 seconds exactly 4 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Left every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Right exactly 5 times
   And user presses Enter exactly 1 times
   And user presses Right every 2 seconds exactly 2 times
   Then element 'Search Result Title' contains text content 'results for "friends"'
   And user presses Enter every 2 seconds exactly 1 times
   And I wait for ad to finish on 'Ad Playing'
   Then element 'Title' has text content 'Friends'
   And element 'Rating' has text content 'G'
   And element 'Synopsis' contains text content 'Ross'
   And user presses Down every 2 seconds exactly 1 times
   And user presses Enter every 3 seconds exactly 1 times
   Then element 'Seasons Button' exists
   And user presses Left every 2 seconds exactly 1 times
   And user presses Up every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 1 times
    And user presses Right every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    And the element 'Left End Duration' is different each time

@smoke
  Scenario: Verify Episode can be played from episodes screen for Automation user profile from first season
    When user presses Right every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 3 seconds exactly 1 times
    And element 'Episodes And Trailers' exists
    And element 'Similar Shows' exists
    And user presses Down every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 1 times
    Then element 'Season' exists
    And element 'Episode Image' is loaded
    And element 'Seasons Metadata' contains text content 'Season 1'
    And user presses Enter every 2 seconds exactly 1 times
    And the element 'Left End Duration' is different each time
    And the element 'Right End Duration' is different each time

  Scenario: Verify user can go to show screen from similar shows list
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 3 seconds exactly 1 times
    And user presses Down every 2 seconds exactly 2 times
    And user presses Enter every 3 seconds exactly 1 times
    Then element 'Similar Shows Header' contains text content 'People who watched'
    And user presses Enter every 1 seconds exactly 1 times
    And 'Play Button' navigation item is focused
    
   @smoke
    Scenario: LG : Verify user can go to show screen from similar shows list using LG mouse
    When user presses Right every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And I move to element 'Hero Belt' 
    And I scroll from element 'Hero Belt' to 'down'
    And I navigate to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 2 seconds exactly 1 times
    And I click on element 'Similar Shows'
    Then element 'Similar Shows Header' contains text content 'People who watched'
    And I click on element 'Similar Shows Page'
    And 'Play Button' navigation item is focused