
Feature: Live Episode and Simulcast test cases

  Scenario: Live TV Plays for Adult Profile
  When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Down every 3 seconds exactly 1 times
    And element 'TV Name' is loaded
    And element 'Episode Title' exists
    And element 'Chevron Up' exists
    And element 'Chevron Down' exists
    And 'TVNZ1' navigation item is selected
    And user presses Up every 2 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    And element 'TV Name' is loaded
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 2 times
    And user presses Up every 3 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Episode Grid EPG' exists on 'Live' screen
    
    @live
    @smoke
  Scenario: Live episode play and AOP on Watch from Start- For show 'Marae'
 When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 10 times
   And user presses Enter
   Then element 'Resume Playing Label' has text content 'Play Live Episode'
   And element 'Badge' has text content 'Live'
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Watch Live' has text content 'WATCH LIVE'
   And element 'Watch From Start' has text content 'WATCH FROM START'
   And user presses Enter every 2 seconds exactly 1 times
   And I long wait for element 'Show Title' exists on 'Play' screen
    And element 'Go To Live' exists
    And element 'ProgressBar' exists
    And 'PlayPause Button' navigation item is focused
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And the element 'Left End Duration' is different each time
    And element 'Left End Duration' has text content not equal to '00:00'
    When element 'PlayPause Button' is paused
    Then element 'AOP Image' is loaded
    And element 'ProgressBar' exists
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    When user presses Up every 2 seconds exactly 1 times
    Then 'CloseAdd' navigation item is focused

  @smoke
  Scenario: Live episode play and AOP on Watch from Live- For show 'Marae'
  When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 10 times
   And user presses Enter
   Then element 'Resume Playing Label' has text content 'Play Live Episode'
   And element 'Badge' has text content 'Live'
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Watch Live' has text content 'WATCH LIVE'
   And element 'Watch From Start' has text content 'WATCH FROM START'
   And user presses Right exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And I long wait for element 'Show Title' exists on 'Play' screen
   And element 'ProgressBar' exists
   And 'PlayPause Button' navigation item is focused
   And 'Go To Live' navigation item is hidden
   And element 'PlayPause Button' exists
   And element 'Back Arrow' exists
   And the element 'Left End Duration' is different each time
   And element 'Left End Duration' has text content not equal to '00:00'
   When element 'PlayPause Button' is paused
   Then element 'AOP Image' is loaded
   And element 'ProgressBar' exists
   And element 'PlayPause Button' exists
   And element 'Back Arrow' exists
   When user presses Up every 2 seconds exactly 1 times
   Then 'CloseAdd' navigation item is focused
   And user presses Enter every 2 seconds exactly 1 times
   And element 'Go To Live' exists
   Then element 'Right Hand Live' has text content 'LIVE'

  Scenario: Go to Live- For show 'Marae'
 When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left exactly 2 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 7 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 10 times
   And user presses Enter
   Then element 'Resume Playing Label' has text content 'Play Live Episode'
   And element 'Badge' has text content 'Live'
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Watch Live' has text content 'WATCH LIVE'
   And element 'Watch From Start' has text content 'WATCH FROM START'
   And user presses Enter every 2 seconds exactly 1 times
   And I long wait for element 'Show Title' exists on 'Play' screen
    And element 'Go To Live' exists
    And 'PlayPause Button' navigation item is focused
   And user presses Up exactly 1 times
   And 'Go To Live' navigation item is focused
    And the element 'Left End Duration' is different each time
   And user presses Enter every 2 seconds exactly 1 times
   And 'Go To Live' navigation item is hidden
    And the element 'Left End Duration' is different each time
    Then element 'Right Hand Live' has text content 'LIVE'


  Scenario: DVR episode play and AOP - For show 'BreakFast'
  When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Right exactly 6 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up exactly 1 times
   And user presses Left exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 6 times
   And user presses Enter every 2 seconds exactly 1 times
   Then element 'Title' has text content 'Breakfast'
  And user presses Enter every 2 seconds exactly 1 times
  And the app will sleep for 5secs
   And the element 'Left End Duration' is different each time
   And the element 'Right End Duration' is different each time
   And 'Go To Live' navigation item is hidden
   And element 'PlayPause Button' is paused
   And element 'AOP Image' is loaded
   And element 'ProgressBar' exists
   And element 'PlayPause Button' exists
   And element 'Back Arrow' exists
   When user presses Up every 2 seconds exactly 1 times
   Then 'CloseAdd' navigation item is focused

   

   
  
  
    