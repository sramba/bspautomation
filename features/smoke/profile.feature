
Feature: Profile feature
  Smoke test includes test cases related to profile

  Scenario: New profile can be created and deleted
   When 'Profile Icons' navigation item is focused
    And user presses Right every 1 seconds exactly 4 times
    And user presses Enter every 1 seconds exactly 5 times
    And user presses Down exactly 3 times
    And user presses Enter every 2 seconds exactly 2 times
    And user presses Down exactly 2 times
    And user presses Right exactly 2 times
    And user presses Enter
    And user presses Left exactly 1 times
    And user presses Enter every 2 seconds exactly 5 times
    And element 'Fifth Profile Name' has text content 'Nnn'
    And user presses Down exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Down exactly 5 times
    And user presses Right exactly 1 times
    And user presses Enter
    And user presses Right exactly 1 times
    And user presses Enter
    Then element 'Add Profile' exists
    

 @profile
  Scenario: First Name is updated for primary profile (After the test case the firstname is updated back.)
    When user presses Down exactly 1 times
    And user presses Enter every 2 seconds exactly 2 times
    And user presses Up exactly 2 times
    And user presses Enter
    And user presses Down every 2 seconds exactly 3 times
    And user presses Enter
    And user presses Down every 2 seconds exactly 5 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'First Profile Name' has text content 'Yeshl'
    When user presses Enter every 2 seconds exactly 2 times
    And user presses Right exactly 3 times
    And user presses Enter
    And user presses Down exactly 1 times
    And user presses Enter
    And user presses Down every 2 seconds exactly 5 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'First Profile Name' has text content 'Yesh'


  Scenario: Verify logout
    When user presses Enter every 3 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left exactly 2 times
    And user presses Down exactly 6 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Down exactly 2 times
    And 'LogOut' navigation item is focused
    And user presses Enter every 2 seconds exactly 1 times
    And 'Cancel' navigation item is focused
    And user presses Right exactly 1 times
    And user presses Enter
    And I long wait for element 'Login page content' exists on 'login' screen

@smoke
@exit
    Scenario: Verify Exit App - STV, LG, Panasonic
    When user presses Enter every 3 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left exactly 2 times
    And user presses Back exactly 1 times
    And element 'Exit Message' contains text content 'To exit the TVNZ+ app, select OK'
    And user presses Right exactly 1 times
    And 'Ok Button' navigation item is focused
    And user presses Enter

  Scenario: Version Check
    When user presses Enter every 3 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left exactly 2 times
    And user presses Down every 2 seconds exactly 7 times
    Then element 'Settings' exists
    And user presses Enter
    And element 'Version' contains text content 'stage-v6.0.8'
