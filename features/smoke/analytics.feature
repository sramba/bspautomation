Feature: To verify analytic calls

  Scenario: Verify Analytics on Login, Play, Follow Content Link for Preschool profile
    When I have get network request to 'Screen' which contains '/choose-profile' is made
    When user presses Right exactly 3 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 3 seconds exactly 4 times
    Then I have get network request to 'Track' which contains 'followContentLink' is made
    And I have get network request to 'Track' which contains '/shows' is made
    And I have get network request to 'Track' which contains 'play' is made
   
  Scenario: Verify Analytics on Pause advert, scrubauto, shows for Preschool profile
   When user presses Right exactly 3 times
    And user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And  I moved over to 'Continue Watching First Tile' element by pressing 'Down' times until it is focused
    And user presses Enter every 3 seconds exactly 4 times
    And the app will sleep for 5secs
    And user presses Enter every 2 seconds exactly 1 times
    And I have get network request to 'Track' which contains 'pause' is made
    When I have get network request to 'Track' which contains 'pause_advert' is made
    When user presses Up every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 2 times
    When I have get network request to 'Track' which contains 'close_advert' is made
    And video is forwarded
    When I have get network request to 'Track' which contains 'scrubAuto' is made
    When user presses Up every 2 seconds exactly 1 times
    When user presses Enter every 2 seconds exactly 1 times
    When I have get network request to 'Track' which contains '/shows' is made

@kk
@smoke
  Scenario: Verify Analytics on Live TV adult profile for TVNZ1,TVNZ2 and Duke
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And the app will sleep for 5secs
    And user presses Down every 2 seconds exactly 1 times
    And user presses Left every 2 seconds exactly 1 times
    And user presses Down every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And the app will sleep for 5secs
    When I have get network request to 'Track' which contains 'play' is made
    When I have get network request to 'Track' which contains 'live' is made
    When I have get network request to 'Screen' which contains '/livetv/tvnz-1' is made
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Down every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    When I have get network request to 'Track' which contains 'abandon' is made
    When I have get network request to 'Track' which contains 'live' is made
    When I have get network request to 'Screen' which contains '/livetv/tvnz-2' is made
    And user presses Enter every 4 seconds exactly 1 times
    And user presses Down every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    Then I have get network request to 'Track' which contains 'abandon' is made
    When I have get network request to 'Track' which contains 'live' is made
    When I have get network request to 'Screen' which contains '/livetv/tvnz-duke' is made


  Scenario: Verify Analytics on Home Screen scroll to right and get a show from there
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Down every 2 seconds exactly 5 times
    And user presses Right every 2 seconds exactly 5 times
    And user presses Enter every 2 seconds exactly 1 times
    And I have get network request to 'Track' which contains 'Scroll' is made
    And I have get network request to 'Track' which contains 'followContentLink' is made
    And I have get network request to 'Screen' which contains '/shows' is made

  Scenario: Verify Analytics on Back to Top for Adult profile
   When user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Main Menu' exists on 'Home' screen
   And I navigate to 'Back To Home' element by pressing 'Down' times until it is focused
   And the app will sleep for 5secs
   And I have get network request to 'Track' which contains 'BackToTop' is made
   And I have get network request to 'Track' which contains 'tvnzhome' is made
   And I have get network request to 'Track' which contains 'module_interactions' is made
  


    


    