
Feature: Play test
 Includes video play test cases

@smoke
  Scenario: Vod can be played and paused on Adults Profile
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 2 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    And I wait for ad to finish on 'Ad Playing'
    Then element 'ProgressBar' exists
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    And element 'Right End Duration' has text content not equal to '00:00'
    And element 'Left End Duration' has text content not equal to '00:00'
    And the element 'Left End Duration' is different each time
    And the element 'Right End Duration' is different each time
    When element 'PlayPause Button' is paused
    Then element 'AOP Image' is loaded
    And element 'ProgressBar' exists
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    When user presses Up every 2 seconds exactly 1 times
    Then 'CloseAdd' navigation item is focused

    Scenario: Verify if Ad comes then it can be paused on Adults Profile
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Down every 2 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 2 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    When I pause or unpause the ad 'Ad Playing' that exists
    Then element 'PlayPause Button' is paused

  @smoke
  Scenario: Vod can be played and paused on Kids Profile
    When user presses Right exactly 2 times
    When user presses Enter every 1 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 2 times
    And user presses Enter every 3 seconds exactly 2 times
    And user presses Up every 3 seconds exactly 1 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    Then element 'ProgressBar' exists
    And user presses Right every 2 seconds exactly 1 times
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    And I wait for ad to finish on 'Ad Playing'
    And the element 'Left End Duration' is different each time
    And the element 'Right End Duration' is different each time
    And element 'Right End Duration' has text content not equal to '00:00'
    And element 'Left End Duration' has text content not equal to '00:00'
    When element 'PlayPause Button' is paused
    Then element 'AOP Image' is loaded
    And element 'ProgressBar' exists
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    When user presses Up every 2 seconds exactly 1 times
    Then 'CloseAdd' navigation item is focused

  Scenario: Vod can be played and paused on Preschooler's profile
    When user presses Right exactly 3 times
    When user presses Enter every 1 seconds exactly 1 times
    Then element 'Main Menu' has text content not equal to 'HEIHEI'
    And user presses Right every 2 seconds exactly 3 times
    And user presses Enter every 2 seconds exactly 2 times
    And user presses Enter every 3 seconds exactly 1 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    Then element 'Show Title' exists
    And element 'ProgressBar' exists
    And user presses Right every 2 seconds exactly 1 times
    And 'PlayPause Button' navigation item is focused
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    And the element 'Left End Duration' is different each time
    And the element 'Right End Duration' is different each time
    And element 'Right End Duration' has text content not equal to '00:00'
    And element 'Left End Duration' has text content not equal to '00:00'
    When element 'PlayPause Button' is paused
    Then element 'AOP Image' is loaded
    And element 'ProgressBar' exists
    And element 'PlayPause Button' exists
    And element 'Back Arrow' exists
    And element 'Captions' exists
    When user presses Up every 2 seconds exactly 1 times
    Then 'CloseAdd' navigation item is focused

  Scenario: Captions can be selected on Preschooler's profile -Danel
  When user presses Right exactly 3 times
   When user presses Enter every 1 seconds exactly 1 times
   Then element 'Main Menu' has text content not equal to 'HEIHEI'
   And user presses Left every 2 seconds exactly 2 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Left every 2 seconds exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Up every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 5 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right every 2 seconds exactly 2 times
   And user presses Enter every 2 seconds exactly 2 times
   And I long wait for element 'Show Title' exists on 'Play' screen
   When user presses Left every 2 seconds exactly 2 times
   When user presses Up every 2 seconds exactly 2 times
   And 'Back Arrow' navigation item is focused
   When user presses Right every 2 seconds exactly 1 times
   When user presses Enter every 2 seconds exactly 1 times
   And I long wait for element 'Caption Selector List' exists on 'Play' screen
   And 'Lang Selector' navigation item is focused

 @s2
 @smoke
  Scenario: Vod can be post/pre played
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 2 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    And I wait for ad to finish on 'Ad Playing'
    And user presses Right every 2 seconds exactly 1 times
    And element 'ProgressBar' exists
    And 'PlayPause Button' navigation item is focused
    And the element 'Right End Duration' is different before and after 'Rewind' the play
    And the element 'Right End Duration' is different before and after 'FAST_FWD' the play
    When user presses Left every 2 seconds exactly 1 times

Scenario: Vod can be post/pre played on PS5
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Main Menu' exists on 'Home' screen
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Enter every 3 seconds exactly 2 times
    And I long wait for element 'Show Title' exists on 'Play' screen
    And I wait for ad to finish on 'Ad Playing'
    And element 'ProgressBar' exists
    And 'PlayPause Button' navigation item is focused
    And the element 'Right End Duration' is different before and after 'D_PAD_LEFT' the play
    And the element 'Right End Duration' is different before and after 'D_PAD_RIGHT' the play

  Scenario: Vod can be played and AOP shown- Xbox (Bluey)
   When user presses Enter every 4 seconds exactly 1 times
   And I long wait for element 'Hero Belt' exists on 'Home' screen
   And user presses Left every 2 seconds exactly 1 times
   And element 'Home Icon' exists
   And user presses Up every 2 seconds exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And element 'Search Result Title' has text content 'Popular Searches'
   And user presses Right exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Down exactly 1 times
   And user presses Left exactly 1 times
   And user presses Enter
   And user presses Up every 2 seconds exactly 2 times
   And user presses Right exactly 4 times
   And user presses Enter
   And user presses Down exactly 2 times
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Right exactly 3 times
   And user presses Enter every 2 seconds exactly 1 times
   And element 'Title' has text content 'Bluey'
   And user presses Down exactly 1 times
   And user presses Enter every 2 seconds exactly 1 times
   And element 'Seasons Button' exists
   And user presses Enter every 2 seconds exactly 1 times
   And user presses Left every 2 seconds exactly 1 times
   And I long wait for element 'Show Title' exists on 'Play' screen
   And I wait for ad to finish on 'Ad Playing'
   And user presses Down every 2 seconds exactly 1 times
   And 'PlayPause Button' navigation item is focused
   And element 'ProgressBar' exists
   And the element 'Right End Duration' is different before and after 'D_PAD_LEFT' the play
   And the element 'Right End Duration' is different before and after 'D_PAD_RIGHT' the play
   And user presses Left every 2 seconds exactly 1 times
   And element 'PlayPause Button' is paused
   Then element 'AOP Image' is loaded
   And element 'ProgressBar' exists
   And element 'PlayPause Button' exists
   And element 'Back Arrow' exists
   And element 'Captions' exists
   And user presses Up every 2 seconds exactly 1 times
   And 'Back Arrow' navigation item is focused

  
  @smoke
    Scenario: LG : Verify the show can be paused and played on Adults Profile using LG mouse
    When I move to element 'Adult Profile'
    And I click on element 'Adult Profile'
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Down every 2 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Captions' exists on 'Play Screen' screen
    And I move to element 'PlayPause Button'
    And I click on element 'PlayPause Button'
    And I click on element 'PlayPause Button'
    And I long wait for element 'Back Arrow' exists on 'Play Screen' screen    
    And I move to element 'PlayPause Button'
    And I click on element 'PlayPause Button'
    And I click on element 'PlayPause Button'