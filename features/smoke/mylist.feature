 
Feature: My List Screen test cases
@smoke

  Scenario: User can add and remove My List from show page
    When user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    When user presses Right every 2 seconds exactly 3 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Add To Favourites' exists on 'Show' screen
    Then element 'Add To Favourites' has text content 'ADD TO My List'
    And user presses Down every 2 seconds exactly 3 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Add To Favourites' has text content 'ADDED TO My List'
    And user presses Enter every 2 seconds exactly 1 times
    And element 'Add To Favourites' has text content 'ADD TO My List'
    And 'Add To Favourites' navigation item is focused

  Scenario: User can view the My List from My list screen
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 4 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Screen Header' has text content 'My List'
    When user presses Enter every 2 seconds exactly 1 times
    Then element 'Add To Favourites' has text content 'ADDED TO My List'

 @smoke
  Scenario: User can see what's new when no lists are there from Kids Profile
    When user presses Right exactly 2 times
    And user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Screen Header' has text content 'My List'
    When user presses Enter every 2 seconds exactly 1 times
    Then 'Top Picks' navigation item is focused

   @smoke
  Scenario: LG : User can view the My List from My List screen
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And I move to element 'LG Mouse Main Menu'
    And I click on element 'LG Mouse Main Menu'
    And I move to element 'Favourites Menu'
    And I click on element 'Favourites Menu'
    And I long wait for element 'My Favourites Title' exists on 'My Favourites Title Header' screen
    And I move to element 'My Favourites Title'
    And I move to element 'My Favourites Hero Belt'
    And I click on element 'My Favourites Hero Belt'
    Then element 'Add To Favourites' has text content 'ADDED TO FAVOURITES'