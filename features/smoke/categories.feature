
Feature: Categories Screen
  Test includes categories screen related test cases from application

@smoke
  Scenario: User can view the Categories tiles from Categories menu
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 5 times
    And 'Categories' navigation item is focused
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Image' exists on 'Category' screen
    When user presses Down every 2 seconds exactly 1 times
    Then element "Category Menu" exists
    And element 'Banner Image' is loaded
    And I verify each categories by pressing 'Right''Left''Down''Enter' to validate 'Banner Image''Featured Belt Title'
    When user presses Right every 2 seconds exactly 5 times
    And element 'Banner Image' is loaded 
    Then element 'Screen Header' exists

 @smoke
 @categories
  Scenario: User can navigate to a show page from categories 
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 1 times
    And user presses Down every 2 seconds exactly 1 times
    And I long wait for element 'Hero Image' exists on 'Category' screen
    When user presses Right every 2 seconds exactly 1 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Play Button' exists on 'Show' screen
    Then element 'Background Image' is loaded
    And 'Play Button' navigation item is focused


  Scenario: Verify Heihei channels on Kids profile
    When user presses Right exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 2 times
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Hero Content' has text content not equal to 'KIDS'
    And 'Top Picks' navigation item is focused


 @categories
 @smoke
  Scenario: LG : Verify Heihei channels on Kids profile using LG mouse
    When I move to element 'Kids Profile'
    And I click on element 'Kids Profile'
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And I move to element 'LG Mouse Main Menu'
    And I click on element 'LG Mouse Main Menu'
    And I move to element 'HeiHei Menu'
    And I click on element 'HeiHei Menu'
    Then element 'Hero Content' has text content not equal to 'KIDS'
    And 'Top Picks' navigation item is focused