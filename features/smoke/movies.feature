Feature: To verify movies screen testcases
Scenario: Navigate to Movies hub, Back To top 
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And user presses Down every 2 seconds exactly 3 times
    And 'Movies Icon' navigation item is focused
    And user presses Enter every 2 seconds exactly 1 times
    Then element 'Hub Header' has text content 'Movies'
    And 'First Item' navigation item is focused
    And element 'Hero Metadata' exists
    And element 'Hero Rating' exists
    And element 'Hero Moods' exists
    And element 'Hero Synopsis' exists
    And I navigate to 'Hub_Back_To_Top' element by pressing 'Down' times until it is focused
    And element 'Hub_Back_To_Top' contains text content 'Back To Top'
    And 'Top Picks' navigation item is focused
    And element 'Banner Image' is loaded

    Scenario: Navigate to subhub from More World Cinema on Movies
    When user presses Enter every 2 seconds exactly 2 times
    And I long wait for element 'Hero Belt' exists on 'Home' screen
    And user presses Left every 2 seconds exactly 2 times
    And I navigate to 'Movies Icon' element by pressing 'Down' times until it is focused
    Then element 'Hub Header' has text content 'Movies'
    And I moved over to 'World Cinema First Tile' element by pressing 'Down' times until it is focused
    And  I navigate to 'More' element by pressing 'Right' times until it is focused
    And 'First Item' navigation item is focused
    When user presses Back exactly 1 times
    Then 'More' navigation item is focused