const selectors = {
   //#region BSP   
    //Login Screen
        'Login page content': {css: '#TVArea > div.t-login.focus-trail > div.left-container.focus-trail > div.wgt-keyboard.keyboard-container.focus-trail > div > div.wgt-button.wgt-keyboard-comp.row-begin.focused > div'},
        'Loading overlay': {css: '#static-loading-screen'},
        'Logo' :{css: '.sprite-tvnz-play'},
        'Login Button' :{css: '.button--secondary'},
        'Email' :{css: '#emailInput'},
        'Password' :{css: '#passwordInput'},
        'Logo' :{css: '.logo'},
        'Next' :{css: '.next-button'},

    //Login Error Screen
        'Error Message' :{css: '.wgt-dialog-container'},
        'Ok Button' :{css: '.ok'},
        'Resend Email' :{css: '.resend'},
        'Label' :{css: '.menu-icon'},

    //Welcome Screen
       'KiaOra Image' :{css: '.profiles-promo__image'},
       'KiaOra Message' :{css: '.profiles-promo__message'},
       
    //Profile Screen
        'Edit Pencil': {css: '.t-profiles-edit__button'},
        'Profile Promo': {css: '.profiles-promo__image'},
        'Profile loaded': {css: '#TVArea > div.t-profiles-switch.focus-trail > div.t-profiles-switch__profiles.focus-trail > div.t-profiles-switch__container.focus-trail > div.t-profiles-switch__button-profile.wgt-button.focused > div.profile-border > img'},
        'First Profile Name': {css: '#TVArea > div.t-profiles-switch.focus-trail > div.t-profiles-switch__profiles.focus-trail > div.t-profiles-switch__container > div:nth-child(1) > div.wgt-label'},
        'First Name Edit Field': {css: '#TVArea > div.t-profiles-manage.focus-trail > div.t-profiles-manage__name.focus-trail > div.t-profiles-manage__name--firstname.wgt-input.wgt-input-div.input > div > pre.wrapperCopy'},
        'Fifth Profile Name': {css: '#TVArea > div.t-profiles-switch.focus-trail > div.t-profiles-switch__profiles.focus-trail > div.t-profiles-switch__container > div:nth-child(5) > div.wgt-label'},
        'Add Profile': {css: '#TVArea > div.t-profiles-switch.focus-trail > div.t-profiles-switch__profiles.focus-trail > div.t-profiles-switch__container.focus-trail > div:nth-child(5)'},
	'Profile Icons': {css: '.t-profiles-switch__button-profile'},
        'Kids Profile': {xpath: '//div[text()="Kid"]'},
        'Adult Profile': {xpath: '//div[text()="Yesh"]'},


    //Left Hand Navigation Menu
        'Main Menu' :{css: '.header-show'},
        'Home Icon':{css:'.home'},
        'Movies Icon':{css:'.movies'},
        'TV Shows Icon':{css:'.tv-shows'},
        'Categories':{css: '#mainMenu > div > div.wgt-grid-row-h.focus-trail > div.categories'},
   
   //Home Screen
        'Continue Watching First Tile': {xpath: '//div[text()="Continue Watching"]/parent::div/div[2]/div/div[1]/div'},
        'Categories First Tile': {xpath: '//div[text()="Categories"]/parent::div/div[2]/div/div[1]/div'},
        'Favourites First Tile': {xpath: '//div[text()="My List"]/parent::div/div[2]/div/div[1]/div'},
        'Movies First Tile': {xpath: '//div[text()="Movies"]/parent::div/div[2]/div/div[1]/div'},
        'Featured Belt Title': {css: '.belt__title'},
        'Hero Title' :{css: '#bannerTitle'},
        'Logged In User Name' :{css: '.main-menu__profile-button > div'},
        'Hero Belt' :{css: '.wgt-show-hero'},
        'Top Picks': {css: '#tile-0'},
        'Explore Channels': {css: '.belt--branded'},
        'Hero Metadata': {css: '.wgt-show-hero__combined-metadata-above-synopsis'},
        'Hero Rating': {css: '.wgt-show-hero__rating'},
        'Hero Moods': {css: '.wgt-show-hero__moods'},
        'Hero Synopsis': {css: '.wgt-show-hero__synopsis'},
        'TV1': {css: '.tile--channel'},
        'Pause button': {css: '.button.pause'},
        'Image': {css: '#imageplayer_img'},
        'Branded Tile': {css: '.tile--branded'},
        'More': {css: '.tile--tertiary'},
        'Banner Image': {css: '.wgt-show-hero__image'},
        'Back To Home': {css: '.tile--back-to-top'},
        'Overlay Ok': {css: '.wgt-overlay__button'},
        'Live Progress Bar': {css: '.tile__live-show-progress-bar'},
        'Home Menu': {css: '.home'},

//Show Screen
        'Background Image': {css: '.t-details__background-image'},
        'Add To Favourites': {css: '.toggle-favourite-gql'},
        'Resume Playing Label': {css: '.t-details__smart-watch > div.wgt-label'},
        'Play Button': {css: '.t-details__smart-watch'},
        'Episodes And Trailers': {css: '.t-details__episodes'},
        'Similar Shows': {css: '.t-details__similar-shows'},
        'Title': {css: '.t-details__title'},
        'Metadata': {css: '.t-details__combined-metadata'},
        'Moods': {css: '.t-details__show-moods-below-metadata-container'},
        'Rating': {css: '.t-details__rating'},
        'Synopsis': {css: '.t-details__synopsis'},
        'Brand Logo': {css: '.t-details__brandedvod-logo'},
        'Seasons Metadata': {css: '.wgt-thumbnail-metadata__right-container'},
        'Seasons Button': {css: '.wgt-seasons__button'},
        'Badge': {css: '.t-details__badge'},
        'Similar Shows Header': {css: '.t-details__left-header'},
        'Page Title': {css: '.t-details__title'},
        'Similar Shows Page': {xpath: '//div[@id="thumbnailMetadataListWgt"]/div[1]/div/div[1]/div/div[1]/img'},
        'Play Screen': {css: '.shared-player__controls-background'},

 
//Episodes Screen   
        'Trailer': {css: '#season-Promo'},
        'Episode Image': {css: '.t-details__background-image'},
        'Season': {css: '#season-1'},
        'Show': {css: '.wgt-thumbnail-metadata__button'},
        'Episode Rating': {css: '.wgt-thumbnail-metadata__rating'},
        'Play Icon': {css: '.icon-play'},
        'Duration': {css: '.wgt-thumbnail-metadata__duration'},
 
//Play Screen
        'Show Title': {css: '.tv-show-title'},
        'Player': {css: '#playerview'},
        'PlayPause Button': {css: '.shared-player__button--play-or-pause'},
        'ProgressBar': {css: '.shared-player__progress__scrubber-button'},
        'Right End Duration': {css: '.shared-player__duration'},
        'Left End Duration': {css: '.shared-player__current-time'},
        'Captions': {css: '.shared-player__current-time'},
        'Captions Title': {css: '.captions-title'},
        'Lang Selector': {css: '.captions-option'},
        'Caption Selector List': {css: '.caption-selector__grid'},
        'Ad Playing': {css: '.ad-is-playing'},
        'Back Arrow': {css: '.backButton'},
 
//AOP Screen
        'AOP Label': {css: '.wgt-adonpause__label'},
        'AOP Image': {css: '.wgt-adonpause__image'},
        'Captions': {css: '.captionsButton'},
        'CloseAdd': {css: '.shared-player__button--cta-button'},
 
//My List Screen
       'Empty Title': {css: '.empty-header'},
       'First Item': {css: '.tile'},
       'LG Mouse Main Menu': {css: '.header-show'},
       'Favourites Menu': {xpath: '//div[@id="mainMenu"]/div/div[4]/div/div[2]'},
       'My Favourites Hero Belt': {css: '.asset-grid > .wgt-grid-scroll > .focus-trail > .wgt-grid-selected > .tile'},
       'My Favourites Title Header': {css: '.t-categories__page-header--label'},
       'Added Favourites': {css: '.toggle-favourite-gql--is-favourite'},
       'My Favourites Title': {css: '.wgt-show-hero__title'},

 //Movies Screen
        'Movies Title': {xpath: '//div[text()="Movies"]'},  
        'World Cinema First Tile': {xpath: '//div[text()="World cinema from Filmstream"]/parent::div/div[2]/div/div[1]/div'},   
        'Hub Header': {css: '.t-hub__page-header--label'},
        'Hub_Back_To_Top': {css: '.t-hub__back-to-top'},

//TV Shows Screen
        'Kids First Tile': {xpath: '//div[text()="Kids"]/parent::div/div[2]/div/div[1]/div'},   
        
//Search Screen
        'Search Result Title': {css: '.t-search__title'},
        'Tile Caption': {css: '.tile__basic-caption'},
        'Search Input': {css: '#searchInput'},
        'Search Field': {css: '.form-input__label'},
        'Input Field': {css: '.wrapper'},

//Categories Screen
        'Category Menu': {css: '.categories-menu__item > div'},
        'Hero Image': {css: '.wgt-show-hero__image-container'},
        'First Tile' :{css:'.categories-top > .asset-grid > .wgt-grid-scroll > .wgt-grid-row-h > :nth-child(1)'},
        'Belt Title' :{css:'//*[@id="tile-0"]/img'},
        'Hero Title': {css: '.show-hero__title'},
        'Brand Logo': {css: '.t-categories__category-logo'},
        'Hero Content': {css: '.wgt-show-hero__details'},
        'Screen Header': {css: '.t-categories__page-header--label'},
        'Movies Category': {xpath: '//div[text()="Movies"][@class="wgt-label"]/parent::div'},
        'Categories Belt Title': {css: '.belt__title'},
        'HeiHei Menu': {xpath: '//div[@id="mainMenu"]/div/div[4]/div'},
    	
//Live TV SimulcastFirst Hero Tile Screen
        'Episode Grid EPG': {css: '#epg-episode-grid'},
        'Episode Title': {css: '.epg-episode__container'},
        'TV Name': {css: '.live-tv-menu__item'},
        'Chevron Up': {css: '.live-tv-menu__chevron-up'},
        'Chevron Down': {css: '.live-tv-menu__chevron-down'},
        'Live Progress Bar': {css: '.live-progress-bar'},
        'TVNZ1': {css: '#menuTile0'},
        'TVNZ2': {css: '#menuTile1'},
        'Duke': {css: '#menuTile3'},
        'Channel Title': {css: '.channel-title'},
 
//Live Video Screen
        'Watch Live': {css: '.watchlive'},
        'Watch From Start': {css: '.watchfromstart'},
        'Go To Live': {css: '.cta-goto-live'},
        'Right Hand Live': {css: '.shared-player__live-container--button-live'},
   
//Settings Screen
        'LogOut': {css: '.logout'},
        'Cancel': {css: '.cancel'},
        'Settings': {css: '.settings'},
        'Version': {css: '.wgt-contact-us__app-version'},

//Exit Screen
        'Exit Message': {css: '.wgt-dialog-message'},  
//#endregion BSP

//#region Andriod

 //Login Screen ---------------------------------------
 'Login Welcome Message_atv': {xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/LinearLayoutCompat/AppCompatTextView[@id="tvLogin_titleView"]'},
 'Login page content_atv': {xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/View[@id="tvLogin_drawerBg"]'},
 'Email_atv' :{css: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/LinearLayoutCompat/KeyboardInputView[@id="tvLogin_emailView"]'},
 'Password_atv' :{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/LinearLayoutCompat/KeyboardInputView[@id="tvLogin_passwordView"]'},
 'Login Text_atv' :{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/LinearLayoutCompat/AppCompatTextView[@id="tvLogin_accountView"]'},
 'Next Button_atv' :{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatButton[@id="tvLogin_actionPrimaryView"]'},
 'Logo_atv': {xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatImageView[@id="tvLogin_iconView"]'},
 'Login Button_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatButton[@id="tvLogin_actionPrimaryView"]'},
 'Previous Button_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatButton[@id="tvLogin_actionSecondaryView"]'},
 'Show Password Button_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatButton[@id="tvLogin_actionPasswordView"]'},

 //Login Error Screen --------------------------------------
 'ErrorMessage_atv':{xpath:'//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatTextView[@id="dialog_alert_title"]'},
 'ErrorMessage_text_atv':{xpath:'//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatTextView[@id="dialog_alert_body"]'},
 
//Verification email error page --------------------------------------
 'Login Button Verify_atv':{css: '#dialog_actionSecondary'},
 'Resend Email Button_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatButton[@id="dialog_actionPrimary"]'},
 'Verification Error Message_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatTextView[@id="dialog_alert_body"]'},
 'Verify Screen Header Message_atv': {xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatTextView[@id="dialog_alert_title"]'},

  //Home screen -------------------------------------------------------------------------
  'Live TV_atv' :{css: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/RecyclerView/ConstraintLayout[1]/ProgressView/AppCompatTextView[@id="tv_progress_text"]'},
  'Hero Top Picks Title_atv': {xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[1]/AppCompatTextView[@id="module_title"]'},
  'Top Picks_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[1]/HorizontalGridView/_LinearLayout[1]/ShowBeltItemView/ConstraintLayout/View[@id="beltitem_selection_border"]'},
  'Hero Metadata_atv':{css: '.wgt-show-hero__combined-metadata-above-synopsis'},
  'Hero Rating_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//HomeHeroView[2]/ConstraintLayout[2]/AppCompatTextView[@id="home_hero_rating"]'},
  'Hero Moods_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//HomeHeroView[2]/AppCompatTextView[@id="home_hero_moods"]'},
  'Hero Synopsis_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//HomeHeroView[2]/AppCompatTextView[@id="home_hero_description"]'},
  'Banner Image_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//HomeHeroView[2]/AppCompatImageView[@id="home_hero_background"]'},
  'Explore Channels_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/HorizontalGridView/_LinearLayout[1]/CategoryBeltItemView/RelativeLayout/RelativeLayout/AppCompatImageView[@id="beltitem_category_image"]'},
  'Belt Title_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/AppCompatTextView[@id="module_title"]'},
  'TV1_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/RecyclerView/ConstraintLayout[1]/View[@id="channel_progress_background"]'},
  'Back To Home_atv': {xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/RelativeLayout/ChangeHandlerFrameLayout/RelativeLayout/FadingEdgeLayout/VerticalGridView/ConstraintLayout/View[@id="back_to_top_selection_border"]'},
  'Category Tile_atv': {xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[3]/HorizontalGridView/_LinearLayout[1]/CategoryBeltItemView/ConstraintLayout/AppCompatTextView[@id="beltitem_category_title"]"]'},
  'Live TV Progress_atv': {xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/RecyclerView/ConstraintLayout[2]/View[@id="channel_progress_background"]'},
  'More Continue Watching_atv' :{css: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/HorizontalGridView/_LinearLayout[2]/MoreBeltItemView/ConstraintLayout/AppCompatTextView[@id="beltitem_more_title"]'},
  'Continue Watching Title_atv' :{css: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//FadingEdgeLayout/VerticalGridView/RelativeLayout[2]/HorizontalGridView/_LinearLayout[2]/MoreBeltItemView/ConstraintLayout/AppCompatTextView[@id="beltitem_more_title"]'},


  //BVOD Categories
  'Brand Logo_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatImageView[@id="categories_brandLogo"]'},

   // Categories
   'Moods_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/ConstraintLayout/AppCompatTextView[@id="categories_moods"]'},

 //Profile Screen
  'Profile Promo_atv': {xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout/ConstraintLayout/RecyclerView/LinearLayout[1]/SwitchProfileImageView[@id="profileItem_image"]'},
 //Live TV
 'Badge EPG_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//ConstraintLayout/HorizontalGridView/ConstraintLayout[1]/LinearLayout/BadgeView[@id="programme_badge"]'},
 'Logo TVNZ1_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/RelativeLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/VerticalGridView/AppCompatImageView[@id="channel_item_logo"]'},
 'Chevron_atv':{xpath: '//DecorView/LinearLayout/FrameLayout/FitWindowsLinearLayout/ContentFrameLayout/FrameLayout/ChangeHandlerFrameLayout/RelativeLayout/ChangeHandlerFrameLayout/FrameLayout/ConstraintLayout/AppCompatImageView[@id="live_down_chevron"]'},
 'Progress EPG_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//ConstraintLayout/HorizontalGridView/ConstraintLayout[1]/ProgressView/ProgressBar[@id="tv_progress_bar"]'},
 'Time EPG_atv':{xpath: '//DecorView//FitWindowsLinearLayout/ContentFrameLayout//ChangeHandlerFrameLayout//ChangeHandlerFrameLayout//ConstraintLayout/HorizontalGridView/ConstraintLayout[2]/LinearLayout/AppCompatTextView[@id="programme_time"]'},
//#endregion Android

}
 
// Return either pre-defined selector or the key itself - maybe it's defined in element repo
module.exports = selector => {
    return selectors[selector] || selector
}