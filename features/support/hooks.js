const { BeforeAll, AfterAll, After, Before } = require('cucumber');
const suitest = require('suitest-js-api');
const { assert } = suitest;
const getSelector = require('../support/selectors');
const retry = require('async-retry');
const { getAppConfig } = require('suitest-js-api');
//Before All scenarios
Before(async () => {
  let appConfig = await getAppConfig();
  switch (appConfig.name) {
    case 'Panasonic Stage':
    await suitest.press(suitest.VRC.SMART, {longPressMs: 1000});
    await suitest.press("RIGHT").repeat(7);
    await suitest.press("ENTER").repeat(2).interval(10000);
    await suitest.sleep(25000);
  break;
  case 'PS4 Stage':
    await suitest.press(suitest.VRC.HOME);
    await suitest.press(suitest.VRC.DOWN);
    await suitest.press(suitest.VRC.OK);
    await suitest.sleep(10000);
    await suitest.press(suitest.VRC.UP).repeat(4);
    await suitest.press(suitest.VRC.RIGHT);
    await suitest.press(suitest.VRC.OK);
    await suitest.sleep(20000);
  break;
  case 'PS5 Stage':
    await suitest.press(suitest.VRC.OK);
    await suitest.sleep(25000);
    break;
  case 'AndroidTV Stage':
      await retry(async () => {
        await suitest.openApp();
      },
        {
          retries: 5,
        });
      await suitest.sleep(30000);
    break; 
  default:
    await retry(async () => {
      await suitest.openApp();
    },
      {
        retries: 5,
      });
    await suitest.sleep(30000);
  }
});

After(async () => {
  let appConfig = await getAppConfig();
  switch (appConfig.name) {
    case 'Panasonic Stage':
      await suitest.press(suitest.VRC.EXIT, {longPressMs: 1000});
      await suitest.sleep(10000);
      break;
    case 'PS4 Stage':
        await suitest.press(suitest.VRC.HOME);
        await suitest.sleep(5000);
        await suitest.press(suitest.VRC.DOWN);
        await suitest.press(suitest.VRC.OPTIONS);
        await suitest.press(suitest.VRC.OK).repeat(2);
        break;
    case 'PS5 Stage':
          await suitest.press(suitest.VRC.HOME);
          await suitest.press(suitest.VRC.OK);
          await suitest.press(suitest.VRC.OPTIONS);
          await suitest.press(suitest.VRC.OK);
          await suitest.sleep(5000);
          break;
    case 'Xbox Stage':
        await suitest.press(suitest.VRC.HOME);
        await suitest.press(suitest.VRC.DOWN).repeat(2);
        await suitest.press(suitest.VRC.MENU);
        await suitest.press(suitest.VRC.DOWN).repeat(2);
        await suitest.press(suitest.VRC.OK);
        break;
    case 'AndroidTV Stage':
        break;
      default:
  await suitest.press(suitest.VRC.EXIT);//Samsung, LG
  await suitest.press(suitest.VRC.RIGHT);
  await suitest.press(suitest.VRC.OK);
  await suitest.sleep(10000);
}});

AfterAll(async () => {
  await suitest.closeSession();
});
//Not Required before this tagged scenarios
Before({ tags: "@smoke and not @logout" }, async () => {
  //console.log('OutSide If. Setting this.loggedIn. Value:' + this.loggedIn);
  //if(!this.loggedIn) {
   await assert.element(getSelector('Login page content')).exists().timeout(10000);
   // And loader is hidden
   await assert.element(getSelector('Loading overlay')).doesNot().exist().timeout(10000);
   await assert.element(getSelector('Logo')).matches(suitest.PROP.IMAGE_LOAD_STATE, suitest.IMAGE_LOAD_STATE.LOADED).timeout(1000);
   await suitest.press("DOWN").repeat(2);
   await suitest.press("RIGHT").repeat(4);
   await suitest.press("ENTER");
   await suitest.press("RIGHT").repeat(3);
   await suitest.press("ENTER");
   await suitest.press("LEFT").repeat(3);
   await suitest.press("ENTER");
   await suitest.press("DOWN");
   await suitest.press("ENTER");
   await suitest.press("DOWN").repeat(3);
   await suitest.press("ENTER");
   await suitest.press("ENTER").repeat(8);
   await suitest.press("DOWN").repeat(4);
   await suitest.press("RIGHT");
   await suitest.press("ENTER").repeat(1).interval(4 * 10000);
   await assert.element(getSelector('Profile Promo')).exist().timeout(1000);
   await suitest.press("ENTER").repeat(1).interval(3 * 1000);
   //await assert.element(getSelector('Profile loaded')).matches(suitest.PROP.IMAGE_LOAD_STATE, suitest.IMAGE_LOAD_STATE.LOADED).timeout(1000);
   //console.log("Inside If. Setting this.loggedIn")
   //this.loggedIn = true;
});

//Not Required before this tagged scenarios
Before({ tags: "@smoke_atv and not @logout_atv" }, async () => {
    await assert.element(getSelector('Login page content_atv')).exists().timeout(10000);
    await suitest.press("DOWN").repeat(1);
    await suitest.press("ENTER").repeat(2);
    await suitest.press("DOWN").repeat(2);
    await suitest.press("RIGHT").repeat(7);
    await suitest.press("ENTER");
    await suitest.press("UP").repeat(2);
    await suitest.press("LEFT");
    await suitest.press("ENTER");
    await suitest.press("DOWN");
    await suitest.press("RIGHT");
    await suitest.press("ENTER").repeat(2);
    await suitest.press("DOWN");
    await suitest.press("LEFT");
    await suitest.press("ENTER");
    await suitest.press("LEFT").repeat(5);
    await suitest.press("UP");
    await suitest.press("ENTER");
    await suitest.press("LEFT");
    await suitest.press("UP");
    await suitest.press("ENTER");
    await suitest.press("DOWN").repeat(6);
    await suitest.press("ENTER");
    await suitest.press("ENTER").repeat(8);
    await suitest.press("DOWN").repeat(5);
    await suitest.press("RIGHT");
    await suitest.press("ENTER").repeat(1).interval(4 * 10000);
    await assert.element(getSelector('Profile Promo_atv')).exist().timeout(1000);
    await suitest.press("ENTER").repeat(1).interval(3 * 1000);
});
