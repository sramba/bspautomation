const urls = {

	'Screen' :'https://api.segment.io/v1/screen',
	'Track' :'https://api.segment.io/v1/track',

};


module.exports = url => {
	return urls[url] || url;
};