const {setDefaultTimeout} = require('cucumber');

setDefaultTimeout(500 * 1000);