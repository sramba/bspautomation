
Feature: Home screen test cases

  Scenario: Home belts loaded successfully, Back To top works
    When element 'Hero Rating_atv' exists
    And element 'Hero Moods_atv' exists
    And element 'Hero Synopsis_atv' exists
    And element 'Hero Top Picks Title_atv' exists
    And 'Top Picks_atv' navigation item is completely displayed
    When user presses Down every 1 seconds exactly 21 times
    When the app will sleep for 5secs
    And 'Back To Home_atv' navigation item is completely displayed
    And 'Top Picks_atv' navigation item is not completely displayed
    When user presses Enter every 2 seconds exactly 1 times
    And 'Banner Image_atv' navigation item is completely displayed
    And element 'Hero Top Picks Title_atv' contains text content 'Top Picks'
    And element 'Back To Home_atv' does not exist

   Scenario: User can navigate to branded channels screen from home and back to home screen
    When element 'Hero Rating_atv' exists
    And user presses Down every 1 seconds exactly 3 times
    And the app will sleep for 5secs
    And element 'Explore Channels_atv' exists
    And element 'Explore Channels Title_atv' contains text content 'Explore Channels'
    And 'Top Picks_atv' navigation item is not completely displayed
    When user presses Enter every 2 seconds exactly 1 times
    Then 'Brand Logo_atv' navigation item is completely displayed
    And 'Moods_atv' navigation item is completely displayed
    When user presses Back every 2 seconds exactly 1 times
    And element 'Explore Channels_atv' exists
    And element 'Brand Logo_atv' does not exist

    Scenario: User can navigate to live tv
    When user presses Down every 1 seconds exactly 8 times
    When user presses Enter every 2 seconds exactly 1 times
    And the app will sleep for 5secs
    And element 'Badge EPG_atv' exists
    Then 'Logo TVNZ1_atv' navigation item is completely displayed
    And 'Chevron_atv' navigation item is completely displayed
    And 'Progress EPG_atv' navigation item is completely displayed
    And 'Time EPG_atv' navigation item is completely displayed

@kk
    Scenario: Verify More tile on Continue Watching Home screen navigates to categories and back from there
    When user presses Down every 1 seconds exactly 1 times
    And the app will sleep for 5secs
    And I navigate to element by pressing 'Right' times until text is 'More Continue Watching' for 'More Continue Watching_atv'
    And user presses Enter every 2 seconds exactly 1 times
    And the app will sleep for 5secs

   


  
  