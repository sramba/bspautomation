
Feature: Login test cases
 Includes login related test cases from AndroidTV
  Scenario:  Unverified account user not able to see vod AndroidTV
    When user presses Right exactly 1 times
    And user presses Down exactly 1 times
    And user presses Enter
    When user presses Down exactly 2 times
    And user presses Right exactly 6 times
    And user presses Enter
    And user presses Up exactly 2 times
    And user presses Left exactly 7 times
    And user presses Enter
    And user presses Down exactly 3 times
    And user presses Right exactly 2 times
    And user presses Enter
    And user presses Down exactly 3 times
    And user presses Enter every 2 seconds exactly 1 times
    And user presses Up exactly 7 times
    And user presses Enter exactly 8 times
    And user presses Down exactly 5 times
    And user presses Right exactly 1 times
    And user presses Enter
    And element 'Verification Error Message_atv' contains text content 'Hold on a sec... please verify your account.'
    And element 'Verification Error Message_atv' contains text content 'You've registered with:b@a.com'
    And element 'Error Message' contains text content 'Check your inbox for our email (spam folders too)'
    And element 'Resend Email Button_atv' exists

  Scenario: Verify login with valid account AndroidTV
    When element 'Login Welcome Message_atv' exists
    And element 'Login Welcome Message_atv' contains text content 'Welcome to'
    And element 'Logo_atv' exists
    When user presses Down
    And user presses Enter exactly 2 times
    And user presses Down exactly 2 times
    And user presses Right exactly 7 times
    And user presses Enter
    And user presses Up exactly 2 times
    And user presses Left
    And user presses Enter
    And user presses Down
    And user presses Right
    And user presses Enter exactly 2 times
    And user presses Down
    And user presses Left
    And user presses Enter
    And user presses Left exactly 5 times
    And user presses Up
    And user presses Enter
    And user presses Left
    And user presses Up
    And user presses Enter
    And user presses Down every 1 seconds exactly 6 times
    And user presses Enter
    And user presses Enter every 1 seconds exactly 8 times
    And user presses down every 1 seconds exactly 5 times
    And user presses Right
    Then element 'Email_atv' contains text content 'aa@grr.la'
    And element 'Password_atv' contains text content '˚˚˚˚˚˚˚˚'
    And user presses Enter every 4 seconds exactly 1 times
   
  Scenario: Verify login with invalid credentials AndroidTV
    When element 'Login page content_atv' exists
    And element 'Logo_atv' exists
    When user presses Down every 1 seconds exactly 3 times
    And user presses Enter every 1 seconds exactly 2 times
    And user presses Right every 1 seconds exactly 7 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Up every 1 seconds exactly 2 times
    And user presses Left every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Down every 1 seconds exactly 1 times
    And user presses Right every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Down every 1 seconds exactly 1 times
    And user presses Left every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Left every 1 seconds exactly 5 times
    And user presses Up every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Left every 1 seconds exactly 1 times
    And user presses Up every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Down every 1 seconds exactly 6 times
    And user presses Enter every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 8 times
    And user presses Down every 1 seconds exactly 5 times
    And user presses Right every 1 seconds exactly 1 times
    And user presses Enter every 1 seconds exactly 1 times
    Then 'Resend Email Button_atv' navigation item is focused now
    And element 'Verification Error Message_atv' contains text content 'The username or password entered was incorrect.'



   
