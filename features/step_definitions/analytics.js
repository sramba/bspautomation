// File contains network-related steps
const {Given, Then, When} = require('cucumber');
const nodeAssert = require('assert');
const fetch = require('node-fetch');
const suitest = require('suitest-js-api');
const {assert} = suitest;
const getNetwork = require('../support/networks');
const { compileFunction } = require('vm');

When('I have get network request to {string} which contains {string} is made', async (url, string) => {
	
	await suitest.assert.networkRequest().contains(getNetwork(url)).requestMatches([{
		name: suitest.NETWORK_PROP.BODY,
		val: string,
		type: suitest.COMP.CONTAIN
	}
	]).wasMade();
});
	When('I have get network request to {string} which contains {string} will be made', async (url, string) => {
		await suitest.assert.networkRequest().contains(getNetwork(url)).requestMatches([{
			name: suitest.NETWORK_PROP.BODY,
			val: string,
			type: suitest.COMP.CONTAIN
		}
		]).willBeMade().timeout(100000);
	
});
When('I have get network response to {string} which contains {string} is made', async (url, string) => {
	
	await suitest.assert.networkRequest().contains(getNetwork(url)).responseMatches([{
		name: suitest.NETWORK_PROP.BODY,
		val: string,
		type: suitest.COMP.CONTAIN,
	}
	]).wasMade();
});