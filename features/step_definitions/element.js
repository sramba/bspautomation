// File contains element-related assertion
const {Then} = require('cucumber');
const suitest = require('suitest-js-api');
const {assert} = suitest;

const nodeAssert = require('assert');

const getSelector = require('../support/selectors');

const DEFAULT_TIMEOUT = 5000;/**
* Helper functions
*/
function isFocused(element) {
   return element.matches(suitest.PROP.CLASS, 'focused', suitest.COMP.CONTAIN);
}

function isSelected(element) {
	return element.matches(suitest.PROP.CLASS, 'wgt-grid-selected', suitest.COMP.CONTAIN);
}

function isHidden(element) {
	return element.matches(suitest.PROP.CLASS, 'hidden', suitest.COMP.CONTAIN);
}

Then('element {string} exists', async selector => {
	// Make sure that element exists in DOM
	await assert.element(getSelector(selector)).exists().timeout(DEFAULT_TIMEOUT);
});

Then('element {string} does not exist', async selector => {
	// Make sure that element exists in DOM
	await assert.element(getSelector(selector)).isNot().exist().timeout(DEFAULT_TIMEOUT);
});

Then('{string} navigation item is focused', async selector => {
	// Element is focused when it contains class "focus"
	await isFocused(assert.element(getSelector(selector))).timeout(DEFAULT_TIMEOUT);
});
Then('{string} navigation item is focused now', async selector => {
	await assert.element(getSelector(selector)).matches(suitest.PROP.HAS_FOCUS,true,suitest.COMP.EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('{string} navigation item is completely displayed', async (selector) => {
	await assert.element(getSelector(selector)).matches(suitest.PROP.IS_COMPLETELY_DISPLAYED,true,suitest.COMP.EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('{string} navigation item is not completely displayed', async (selector) => {
	await assert.element(getSelector(selector)).matches(suitest.PROP.IS_COMPLETELY_DISPLAYED,false,suitest.COMP.EQUAL).timeout(DEFAULT_TIMEOUT);
});


Then('{string} navigation item is hidden', async selector => {
	// Element is focused when it contains class "hidden"
	await isHidden(assert.element(getSelector(selector))).timeout(DEFAULT_TIMEOUT);
});

Then('{string} navigation item is selected', async selector => {
	// Element is selected when it has class "active"
	await isSelected(assert.element(getSelector(selector))).timeout(DEFAULT_TIMEOUT);
});

Then('element {string} is loaded', async selector => {
	// Make sure that given image is loaded, give it up to 5s to load
	await assert.element(getSelector(selector)).matches(suitest.PROP.IMAGE_LOAD_STATE, suitest.IMAGE_LOAD_STATE.LOADED).timeout(DEFAULT_TIMEOUT);
});

Then('element {string} has property {string} equals {string}', async (selector, property, value) => {
	// Make sure that element property equals to given value
	await assert.element(getSelector(selector)).matches(property, value, suitest.COMP.EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('element {string} has text content {string}', async (selector, text) => {
	// Make sure that element property  has text equals to given value
	await assert.element(getSelector(selector)).matches(suitest.PROP.TEXT_CONTENT, text, suitest.COMP.EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('element {string} contains text content {string}', async (selector, text) => {
	// Make sure that element property  has text equals to given value
	await assert.element(getSelector(selector)).matches(suitest.PROP.TEXT_CONTENT, text, suitest.COMP.CONTAIN).timeout(DEFAULT_TIMEOUT);
});

Then('element {string} has text content not equal to {string}', async (selector, text) => {
	// Make sure that element property  contains this text
	await assert.element(getSelector(selector)).matches(suitest.PROP.TEXT_CONTENT, text, suitest.COMP.NOT_EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('the element {string} is different before and after {string} the play', async (selector,key) => {
	const element = await suitest.element(getSelector(selector));
	const beforeForward = element.text;
	await suitest.assert.press(key.toUpperCase()).repeat(5);	
	await suitest.assert.press(suitest.VRC.OK);	
	await assert.element(getSelector(selector)).matches(suitest.PROP.TEXT_CONTENT, beforeForward, suitest.COMP.NOT_EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then('the element {string} is different each time', async (selector) => {
	const element = await suitest.element(getSelector(selector));
	const beforeForward = element.text;	
	console.log(beforeForward)	
	await assert.element(getSelector(selector)).matches(suitest.PROP.TEXT_CONTENT, beforeForward, suitest.COMP.NOT_EQUAL).timeout(DEFAULT_TIMEOUT);
});

Then ('I verify each categories by pressing {string}{string}{string}{string} to validate {string}{string}',async (key1,key2,key3,key4,selector,selector1) => {
	for (let step = 0; step < 10; step++) {
		await suitest.assert.press(key1.toUpperCase());
		await suitest.sleep(DEFAULT_TIMEOUT);
		await assert.element(getSelector(selector)).matches(suitest.PROP.IMAGE_LOAD_STATE, suitest.IMAGE_LOAD_STATE.LOADED).timeout(10000);
		await suitest.assert.press(key2.toUpperCase());
		await assert.element(getSelector(selector1)).exists().timeout(DEFAULT_TIMEOUT);
		await suitest.sleep(DEFAULT_TIMEOUT);//Sleep needed as the page loading slow
		await suitest.assert.press(key3.toUpperCase());
		await suitest.sleep(DEFAULT_TIMEOUT);
		await suitest.assert.press(key4.toUpperCase());
	}
	});
	
