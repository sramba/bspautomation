// File contains element-related actions and Givens
const {Given, Then, When} = require('cucumber');
const nodeAssert = require('assert');
const fetch = require('node-fetch');
const suitest = require('suitest-js-api');
const {assert} = suitest;

const getSelector = require('../support/selectors');
const getTestId = require('../support/testEditorTests');

// How long should we wait for element assertions
const DEFAULT_TIMEOUT = 5000;

/**
 * Helper functions
 */
function isFocused(element) {
	return element.matches(suitest.PROP.CLASS, 'focused', suitest.COMP.CONTAIN);
}

function isSelected(element) {
	return element.matches(suitest.PROP.CLASS, 'wgt-grid-selected', suitest.COMP.CONTAIN);
}


// function isAdPlaying(element) {
// 	return element.matches(suitest.PROP.CLASS, 'ad-is-playing', suitest.COMP.CONTAIN);
// }

Given('testcase is exported', async () => {
console.log('Step -1 is executing');
});

Then('Test result is imported', async () => {
console.log('Step -2 is executing');

});
When('the app will sleep for 5secs', async () => {
	await suitest.sleep(5000);
});

When('I navigate to element by pressing {string} times until {string}', async (key,selector) => {
	await suitest.assert.press(key.toUpperCase()).interval(1000).repeat(25).until(
		await assert.element(getSelector(selector)));

});

When('I long wait for element {string} exists on {string} screen', async (selector, screenName) => {
	// Make sure that element exists in DOM
	await assert.element(getSelector(selector)).exists().timeout(50000);
});

When('I navigate to {string} element by pressing {string} times until it is focused', async (selector, key) => {
	await suitest.assert.press(key.toUpperCase()).interval(1000).repeat(25).until(
		isFocused(suitest.element(getSelector(selector))));
	await suitest.press(suitest.VRC.OK);

});

When('I moved over to {string} element by pressing {string} times until it is focused', async (selector, key) => {
	await suitest.assert.press(key.toUpperCase()).interval(1000).repeat(25).until(
		isFocused(suitest.element(getSelector(selector))));

});
When('I moved over to {string} element by pressing {string} times until it is selected', async (selector, key) => {
	await suitest.assert.press(key.toUpperCase()).interval(1000).repeat(20).until(
		isSelected(suitest.element(getSelector(selector))));

});
When('I pause or unpause the ad {string} that exists', async (selector) => {
	if (await suitest.element(getSelector(selector)).exists()) {
		await suitest.assert.press(suitest.VRC.OK).repeat(1).interval(5 * 1000);	
	}		
});
When('I wait for ad to finish on {string}', async (selector) => {
await suitest.element(getSelector(selector)).doesNot()
		.exist().timeout(10000);
			
});

When('I click on element {string}', async (selector) => {
	await suitest.element(getSelector(selector)).click();
				
});
When('I scroll from element {string} to {string}', async (selector,key) => {
	await suitest.element(getSelector(selector)).scroll(key);
			
});
When('I move to element {string}', async (selector) => {
	await suitest.element(getSelector(selector)).moveTo();
				
});

When('element {string} is paused', async selector => {
	if (await suitest.element(getSelector(selector)).exists().timeout(DEFAULT_TIMEOUT)) {
		await suitest.assert.press(suitest.VRC.OK);	}
});

When('video is forwarded', async () => {
	await suitest.assert.press(suitest.VRC.FAST_FWD).repeat(2).interval(2000);	
});

When('I exit app', async () => {
	await suitest.press(suitest.VRC.EXIT);
	await suitest.press(suitest.VRC.OK);
	await suitest.sleep(10000);
	
});

Then('run Test Editor snippet {string}', async testName => {
	// Run pre-defined test editor test snippet
	await suitest.assert.runTest(getTestId(testName));
});
